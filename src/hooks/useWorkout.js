import {useState} from "react";

const useWorkout = (dataForWorkout) => {

    const [word, setWord] = useState('');
    const [translation, setTranslation] = useState('');
    const [similarWords, setSimilarWords] = useState([]);

    const [jumbledTranslation, setJumbledTranslation] = useState('');

    const [currentWorkoutPage, setCurrentWorkoutPage] = useState(1);
    const [numCorrectAnswers, setNumCorrectAnswers] = useState(0);

    const changeCurrentWorkoutPage = () => {
        setCurrentWorkoutPage(currentWorkoutPage => ++currentWorkoutPage);
    }

    const changeNumCorrectAnswers = () => {
        setNumCorrectAnswers(numCorrectAnswers => ++numCorrectAnswers);
    }

    const getDataForWorkoutPage = () => {
        const currentPage = currentWorkoutPage - 1;
        const word = dataForWorkout[currentPage].word;
        const translation = dataForWorkout[currentPage].translation.word;
        const similarWords = dataForWorkout[currentPage].similar_words;


        if (!similarWords.includes(translation)){
            similarWords.push(translation);
            shuffle(similarWords);
        }

        const arr = [];
        for (let i = 0; i < translation.length; i++){
            arr.push(translation[i])
        }
        shuffle(arr);

        setJumbledTranslation(arr.toString().replace(/,/g, ''));
        setWord(word);
        setTranslation(translation);
        setSimilarWords(similarWords);


    }

    // перемешивание элементов в массиве методом Фишера — Йетса
    const shuffle = (array) => {
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    return {
        word,
        translation,
        similarWords,
        currentWorkoutPage,
        numCorrectAnswers,
        jumbledTranslation,
        setCurrentWorkoutPage,
        changeCurrentWorkoutPage,
        changeNumCorrectAnswers,
        getDataForWorkoutPage
    }
}

export default useWorkout;