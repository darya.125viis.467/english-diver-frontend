import {useEffect, useState} from "react";
import WorkoutService from "../services/workout-service";

const UseHttpWorkout = (WorkoutID) => {

    const [dataForWorkout, setDataForWorkout] = useState(null);
    const [answerList,setAnswerList] = useState([]);

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    useEffect( () => {
        getDataToServer();
    }, [])

    const getDataToServer = () => {
        setLoading(true);
        setError(false);
        WorkoutService.getDataForWorkout(WorkoutID)
            .then(response => {
                if(response.statusText !== "OK"){
                    throw new Error(`Failed to complete the request: ${response.config.url}. 
                    Error status: ${response.status}`);
                }
                setLoading(false);
                setError(false);
                setDataForWorkout(response.data);
        })
            .catch(err => {
                setLoading(false);
                setError(err.response);
                throw err;
            })
    }

    const changeAnswerList = (newElement) => {
        return setAnswerList([newElement, ...answerList]);
    }

    const sendResultsToServer = () => {
        WorkoutService.endOfWorkout(WorkoutID, answerList);
    }

    return {
        dataForWorkout,
        setAnswerList,
        answerList,
        changeAnswerList,
        sendResultsToServer,
        loading,
        error
    }
}

export default UseHttpWorkout;