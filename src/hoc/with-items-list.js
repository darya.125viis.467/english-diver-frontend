import {useEffect, useState} from "react";
import LoadingInitialState from "../components/loading/loading-initial-state/loading-initial-state";

const withItemsList = (WrappedComponent, selectData) => {
    return (props) => {

        const [itemsList, setItemsList] = useState([]);
        const [loading, setLoading] = useState(false);

        useEffect(() => {
            setLoading(true);
            selectData().then(res => {
                const itemsList = res.data.results;
                setItemsList(itemsList);
                setLoading(false)
            })
        }, [])

        const loadingElement = loading ? <LoadingInitialState/> : null;

        return (
            <>
                {loadingElement}
                <WrappedComponent {...props}
                                    itemsList={itemsList}/>
            </>
        )
    }
}

export default withItemsList;