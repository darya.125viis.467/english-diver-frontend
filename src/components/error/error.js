import "./error.css"
import errorImg from "../../images/error.png"
import {NavLink} from "react-router-dom";


const Error = ({error}) => {

    const notEnoughWordsMessage =
        <>
            Минимальное количество слов для тренировке: 5.
            Добавить слова можно в разделе <NavLink to="/word-sets" className="link">"Наборы слов"</NavLink>.
        </>

    const requestErrorMessage = `Ошибка загрузки. Код ошибки: ${error.status}`;

    const errorMessage = error.status === 400 ? notEnoughWordsMessage : requestErrorMessage;

    return (
        <div className="error">
            <div className="row">
                <img src={errorImg} alt="error"/>
            </div>
            <div className="row error-txt">{errorMessage}</div>
        </div>
    )
}

export default Error;