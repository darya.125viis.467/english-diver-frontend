import {Link} from "react-router-dom";

import './navigation-buttons.css';

const NavigationButtons = ({firstButtonName, firstLink, secondButtonName, secondLink}) => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-6 col-xs-6 col-btn">
                    <div className="allprojects-bg">
                        <Link to={firstLink} className="btn-ltstlk" role="button">{firstButtonName}</Link>
                    </div>
                </div>

                <div className="col-md-6 col-xs-6 col-btn">
                    <div className="allprojects-bg">
                        <Link to={secondLink} className="btn-ltstlk" role="button">{secondButtonName}</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NavigationButtons;