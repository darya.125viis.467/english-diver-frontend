import './auth-footer.css'

const AuthFooter = () => {

    return(
        <div className="container">
            <div className="row content-authorization">

                <div className="col-md-4 col-xs-12">
                    <div className="shw-cs3">Служба поддержки</div>
                    <a href="mailto:#0">
                        <div className="ftr-lnk"> darya.125viis.467@gmail.com</div>
                    </a>
                </div>

                <div className="col-md-4 col-xs-12 links">
                    <a target="_blank" href="http://twitter.com/">
                        <div className="ftr-lnk"> Twitter</div>
                    </a>
                    <a target="_blank" href="http://instagram.com/">
                        <div className="ftr-lnk"> Instagram</div>
                    </a>
                    <a target="_blank" href="http://pinterest.com/">
                        <div className="ftr-lnk"> Pinterest</div>
                    </a>
                </div>

            </div>
        </div>
    )
}

export default AuthFooter;