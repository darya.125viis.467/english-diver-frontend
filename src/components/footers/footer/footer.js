import {Link} from "react-router-dom";

import './footer.css';

const Footer = () => {

    return (
        <div className="container">
            <div className="row">
                <div className="footer-bg">
                    <div className="col-md-4 col-xs-12">
                        <div className="shw-cs3"> Служба поддержки</div>
                        <a href="mailto:#0">
                            <div className="ftr-lnk"> darya.125viis.467@gmail.com</div></a>
                    </div>

                    <div className="col-md-4 col-xs-6">
                        <a target="_blank" href="http://twitter.com/">
                            <div className="ftr-lnk"> Twitter</div>
                        </a>
                        <a target="_blank" href="http://instagram.com/">
                            <div className="ftr-lnk"> Instagram</div>
                        </a>
                        <a target="_blank" href="http://pinterest.com/">
                            <div className="ftr-lnk"> Pinterest</div>
                        </a>
                    </div>

                    <div className="col-md-4 col-xs-6">
                        <Link to="/word-sets">
                            <div className="ftr-lnk"> Наборы слов</div>
                        </Link>
                        <Link to="/user-dictionary">
                            <div className="ftr-lnk"> Мой словарь</div>
                        </Link>
                        <Link to="/registration">
                            <div className="ftr-lnk"> Выход</div>
                        </Link>
                    </div>


                </div>
            </div>
        </div>
    );
}

export default Footer;