import {useEffect, useState} from "react";

import './content-workout.css';

const ContentWorkout = ({
                            russianWord,
                            translation,
                            changeAnswerList,
                            changeCurrentWorkoutPage
}) => {
    const correctAnswer = "Правильно!";
    const incorrectAnswer = `Неверно. Правильный ответ ${translation}`;
    const noAnswer = "Пожалуйста, введите слово"

    const [value, setValue] = useState('');
    const [result, setResult] = useState(null);
    const [ seconds, setSeconds ] = useState(1);

    const handleChange = (e) =>{
        setValue(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        showResult();
    }

    useEffect(() => {
        if (seconds > 0 && result) {
            setTimeout(setSeconds, 100, seconds - 1);
        } else if (seconds === 0) {
            changeCurrentWorkoutPage();
            setResult(null);
            setValue('');
        }
    }, [seconds]);

    const showResult = () => {
        if(value){
            if (value.toLowerCase().replace(/ /g, '') === translation){
                setResult(correctAnswer);
                changeAnswerList({"word": translation, "is_true": true});
                setSeconds(15);
            } else {
                setResult(incorrectAnswer);
                changeAnswerList({"word": translation, "is_true": false});
                setSeconds(15);
            }
        } else {
            setResult(noAnswer);
        }
    }

    return (
        <form name="multi" onSubmit={handleSubmit}>
            <div className="row word-for-translation-q">
                {russianWord}
            </div>
                <label className="label">Введите перевод слова</label>
                <input className="form-control form-control-lg form" value={value} onChange={handleChange}/>
                <input className="btn-modal-form" type="submit" value="Проверить"/>
            <div className="row result-form">{result}</div>
        </form>
    )
}

export default ContentWorkout;