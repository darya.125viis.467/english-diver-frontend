import { useDrag } from 'react-dnd';
import {useEffect, useState} from "react";

import "./letter-to-drag.css";

const LetterToDrag = (props) => {
    const [didDrop, setDidDrop] = useState(false);

    useEffect(() => {
        if(props.idLetter === props.letter+props.id){
            setDidDrop(false);
        }
    }, [props.idLetter])

    useEffect(() => {
        if(props.newAttemptBool){
            setDidDrop(false)
        }
    }, [props.newAttemptBool])

    const [{isDragging}, drag] = useDrag(() => ({
        type: 'letter',
        item: {letter: props.letter, id: props.letter+props.id},
        collect: monitor => ({
            isDragging: !!monitor.isDragging(),
        }),
        end(item, monitor) {
            const didDrop = monitor.didDrop();
            setDidDrop(didDrop);
        },
    }))

    return(
        <div ref={drag} key={props.letter+props.id}
             className={didDrop ? "letter-after-drag col-sm-1" : "letter-before-drag col-sm-1"}
             style={{opacity: isDragging ? 0.5 : 1}}>
            {props.letter}
        </div>
    )
}

export default LetterToDrag;