import {useDrop} from "react-dnd";
import {useEffect, useState} from "react";

const CellForDrop = ({
                         rightLetter,
                         setIdLetter,
                         increaseNumCorLetters,
                         increaseCellMass,
                         decreaseNumCorLetters,
                         decreaseCellMass,
                         showAnswer,
                         newAttemptBool
}) => {

    const cellColor = '#aee1d18f';
    const hoverColor = '#6cffe3';
    const errorColor = '#d9534fd1';

    const [itemState, setItemState] = useState('');
    const [backgroundColor, setBackgroundColor] = useState(null);

    useEffect(() => {
        if(newAttemptBool){
            setBackgroundColor(cellColor);
            setItemState('');
        }
    }, [newAttemptBool])

    const moveLetter = (item) => {
        setItemState(item);
        increaseCellMass();
        setIdLetter(null);

        if(rightLetter===item.letter){
            increaseNumCorLetters();
        }
    }

    const hideLetter = () => {
        setItemState('');
        decreaseCellMass();
        setIdLetter(itemState.id);

        if(rightLetter===itemState.letter){
            decreaseNumCorLetters();
        }
    }

    const [{ isOver }, drop] = useDrop(() => ({
        accept: 'letter',
        drop(item) {
            moveLetter(item);
        },
        collect: monitor => ({
            isOver: monitor.isOver() && !itemState,
        }),
        canDrop: () => {
            return !itemState;
        },
    }), [itemState])

    useEffect(() => {
        setBackgroundColor(isOver ? hoverColor : cellColor);
    }, [isOver])

    useEffect(() => {
        if(showAnswer && rightLetter!==itemState.letter){
            setBackgroundColor(errorColor);
        }
    }, [showAnswer])

    return (
        <div ref={drop}
             className="col-sm-1"
             style={{
                 backgroundColor,
                 width: '12%',
                 height: '50px',
                 border: 'solid',
                 padding: '10px',
                 margin: '5px',
                 position: 'relative',
             }}
             onClick={hideLetter}>
            {itemState.letter}
        </div>
    )
}

export default CellForDrop;