import {useEffect, useState} from "react";

import LetterToDrag from "../letter-to-drag/letter-to-drag";
import CellForDrop from "../cell-for-drop/cell-for-drop";
import ViewInitialStateLoading from "../../../loading/loading-initial-state/loading-initial-state";
import Error from "../../../error/error";

import "./content-workout.css"

const ContentWorkout = ({
                            error,
                            loading,
                            translation,
                            russianWord,
                            jumbledTranslation,
                            changeAnswerList,
                            changeNumCorrectAnswers,
                            changeCurrentWorkoutPage
}) => {
    const incorrectAnswerMess = "Неправильно :с";
    const tryAgainMess = "Неверный ответ. Попробуйте ещё раз!";
    const correctAnswerMess = "Правильно!";

    const [newAttemptBool, setNewAttemptBool] = useState(false);
    const [numWrongAttempts, setNumWrongAttempts] = useState(0);
    const [showAnswer, setShowAnswer] = useState(null);

    const [numCorrectLetters, setNumCorrectLetters] = useState(1);
    const [cellMass, setCellMass] = useState(0);
    const [idLetter, setIdLetter] = useState(null);

    const [seconds, setSeconds] = useState(1);

    const increaseNumCorLetters = () => {
        setNumCorrectLetters(numCorLetters => numCorLetters+1);
    }

    const decreaseNumCorLetters = () => {
        setNumCorrectLetters(numCorLetters => numCorLetters-1);
    }

    const decreaseCellMass = () => {
        setCellMass(cellMass => cellMass-1);
    }

    const increaseCellMass = () => {
        setCellMass(cellMass => cellMass+1);
    }

    useEffect(() => {
        if (seconds > 0 ) {
            setTimeout(setSeconds, 100, seconds - 1);
        } else if (seconds === 0 && showAnswer) {
            if(showAnswer === tryAgainMess){
                setShowAnswer(null);
                setNewAttemptBool(true);
                setNumCorrectLetters(1);
                setCellMass(0);
                setIdLetter(null);
                setNumWrongAttempts(numWrongAttempts => numWrongAttempts+1);
            } else {
                setNumWrongAttempts(0);
                setShowAnswer(null);
                setNewAttemptBool(true);
                setNumCorrectLetters(1);
                setCellMass(0);
                setIdLetter(null);
                if(showAnswer===incorrectAnswerMess){
                    changeAnswerList({"word": translation, "is_true": false})
                }
                changeCurrentWorkoutPage();
            }
        }
    }, [seconds]);

    useEffect(() => {
        if(translation.length){
            if (cellMass === translation.length &&
                numCorrectLetters === translation.length)
            {
                setNumWrongAttempts(0);
                setNewAttemptBool(false);
                setSeconds(18);
                setShowAnswer(correctAnswerMess);
                changeNumCorrectAnswers();
                changeAnswerList({"word": translation, "is_true": true})
            } else if (cellMass === translation.length &&
                       numCorrectLetters < translation.length)
            {
                setNewAttemptBool(false);
                setSeconds(18);
                numWrongAttempts < 1 ? setShowAnswer(tryAgainMess) : setShowAnswer(incorrectAnswerMess);
            }
        }
    }, [cellMass, setCellMass])

    const getLetters = () => {
        const letters = [];
        for (let i = 0; i < jumbledTranslation.length; i++){
            letters.push(<LetterToDrag letter={jumbledTranslation[i]}
                                       id={i}
                                       key={jumbledTranslation[i]+ i}
                                       idLetter={idLetter}
                                       newAttemptBool={newAttemptBool}
            />)
        }
        return letters;
    }

    const getCells = () => {
        const cells = [];
        for (let i = 0; i < translation.length; i++){
            cells.push(<CellForDrop key={translation[i]+ i}
                                    rightLetter={translation[i]}
                                    setIdLetter={setIdLetter}
                                    increaseNumCorLetters={()=>increaseNumCorLetters()}
                                    decreaseNumCorLetters={()=>decreaseNumCorLetters()}
                                    increaseCellMass={()=>increaseCellMass()}
                                    decreaseCellMass={()=>decreaseCellMass()}
                                    showAnswer={showAnswer}
                                    newAttemptBool={newAttemptBool}/>);
        }
        return cells;
    }

    const loadingElement = loading ? <ViewInitialStateLoading/> : null;
    const errorElement = error ? <Error error={error}/>: null;

    return(
        <>
            {loadingElement}
            {errorElement}
            <div className="row russian-word">
                {russianWord}
            </div>
            <div className="row centered">
                {getCells()}
            </div>
            <div className="row centered">
                {getLetters()}
            </div>
            <div className="answer">
                {showAnswer}
            </div>
        </>
    )
}

export default ContentWorkout;