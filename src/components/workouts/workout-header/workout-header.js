import "./workout-header.css";

const WorkoutHeader = ({loading, header, totalNumber, number, styleHeader}) => {
    return (
        <div className="row">
            <div className="col-md-12">
                <div className={styleHeader}>
                    {loading ?
                        <h2> {header} </h2> :
                        <h2> {header} {number}/{totalNumber} </h2>}
                </div>
            </div>
        </div>
    )
}

export default WorkoutHeader;