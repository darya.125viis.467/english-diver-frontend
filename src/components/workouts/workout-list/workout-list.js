import WorkoutListItem from "../workout-list-item/workout-list-item";

const WorkoutList = ({itemsList}) =>{

    const getElement = () => {
            return itemsList.map((item) => {
                return  <WorkoutListItem key={item.id}
                                         id={item.id}
                                         heading={item.name}
                                         description={item.description}
                                         image={item.image}
                />
            });
    }

    return (
        <div className="row">
            {getElement()}
        </div>
    );

}

export default WorkoutList;