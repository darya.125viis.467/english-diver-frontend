import {Link} from "react-router-dom";

import "./workout-result.css";

const WorkoutResult = ({numCorrectAnswers, totalWorkoutPage, styleContent, styleButton}) => {
    return (
        <div className="row">
            <div className='works-hero-results'/>
            <div className={`${styleContent} results`}>
                Ваши результаты!
                <div className="indents">
                    {numCorrectAnswers} правильных ответов из {totalWorkoutPage}.
                </div>
                <Link to="/">
                    <button className={`btn btn-primary ${styleButton}`}>
                        Завершить тренировку
                    </button>
                </Link>
            </div>
            <div className="indents"/>
        </div>
    )
}

export default WorkoutResult;