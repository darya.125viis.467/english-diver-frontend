import {Link} from "react-router-dom";

import './workout-list-item.css';

const WorkoutListItem = ({id, heading, description, image}) => {

    return (
        <div className="col-md-6 col-xs-12 col-item">
            <div className="prjct-bg">
                <Link to={`/${id}`}>
                    <img src={image} alt="" className="img-prjct-wrk"/>
                </Link>
                <div className="prjct-wrt-left-wrk"/>
                <div className="shw-cs2"> {heading} </div>
                <div className="shw-cs"> {description} </div>
            </div>
        </div>
    )
}

export default WorkoutListItem;