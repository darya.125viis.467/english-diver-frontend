import { useState } from "react";

import LoadingInitialState from "../../../loading/loading-initial-state/loading-initial-state";
import Error from "../../../error/error";

import './content-workout.css';

const ContentWorkout = (props) => {

    const [displayButton, setDisplayButton] = useState("none");
    const [colorButton, setColorButton] = useState("");
    const [message, setMessage] = useState("");
    const [disabled, setDisabled] = useState(false);
    const [showTranslation, setShowTranslation] = useState(false);

    const onToggleShowTranslation = () => {
        setShowTranslation(showTranslation => !showTranslation);
    }

    const showResult = (selectedWord) => {
        if (selectedWord === props.translation) {
            setColorButton("green");
            setMessage("Верно!");

            props.changeNumCorrectAnswers();

            props.changeAnswerList({"word": props.translation, "is_true": true})

        } else {
            setColorButton("red");
            setMessage("Неправильно.");

            props.changeAnswerList({"word": props.translation, "is_true": false})
        }
        setDisplayButton("block");
        setDisabled(true);
        onToggleShowTranslation();
    }

    const getElements = () => {
        return props.words.map((item) => {
            return (
                <button key={item} onClick={() => showResult(item)}
                        className="btn btn-primary btn-modal"
                        disabled={disabled}>{item}</button>
            )
        })
    }

    const goNextWorkoutPage = () => {
        props.changeCurrentWorkoutPage();

        setDisplayButton("none");
        setDisabled(false);

        onToggleShowTranslation();
    }

    const loading = props.loading ? <LoadingInitialState/> : null;
    const error = props.error ? <Error error={props.error}/>: null;

    return (
        <>
            {loading}
            {error}
            <div className="col-md-6 col-xs-12 word-for-translation">
            {props.russianWord}
            {showTranslation ?
                <div className="translation-word">
                    {props.translation}
                </div>
                : null}
            </div>
            <div key={props.key} className="col-md-6 col-xs-12 translations">
                {getElements()}
                <button onClick={goNextWorkoutPage}
                        style={{ display: displayButton, background: colorButton }}
                        className="btn btn-modal">{message}</button>
            </div>
        </>
    )
}

export default ContentWorkout;

