import {useEffect, useRef} from "react";

import "./timer.css"

const Timer = ({
                   correctAnswerBool,
                   setCorrectAnswerBool,
                   currentWorkoutPage,
                   setResponseMessage
}) => {
    const deadline = new Date(Date.parse(new Date()) + 5 * 1000);
    const timerInterval = useRef();

    const smilingEmoticon = "🥳";
    const sadEmoticon = "🧐"
    const timeEmoticon = "⌛"

    useEffect(() => {
        setResponseMessage("");
        initializeClock("countdown", deadline);
    }, [currentWorkoutPage])

    useEffect(() => {
        if(correctAnswerBool === true){
            document.getElementById("countdown").innerHTML = smilingEmoticon;
            setCorrectAnswerBool(null);
        } else if (correctAnswerBool === false) {
            setCorrectAnswerBool(null);
            document.getElementById("countdown").innerHTML = sadEmoticon;
        }
        clearInterval(timerInterval.current)
    }, [correctAnswerBool])


    const getTimeRemaining = (endTime) => {
        let t = Date.parse(endTime) - Date.parse(new Date());
        let seconds = Math.floor((t / 1000) % 60);
        return {
            total: t,
            seconds: seconds,
        }
    }

    const initializeClock = (id, endTime) => {
        const clock = document.getElementById(id);

        function updateClock() {
            const t = getTimeRemaining(endTime);

            if (t.total <= 0) {
                clearInterval(timerInterval.current);
                setResponseMessage("Время на ответ вышло");
                document.getElementById("countdown").innerHTML = timeEmoticon;
                return true;
            }
            clock.innerHTML = t.seconds;
        }
        updateClock();
        timerInterval.current = setInterval(updateClock, 1000);
    }

    return (
        <div className="row">
            <div className="col-md-12">
                <div className="timer-background">
                    <div className="timer-text" id="countdown"/>
                </div>
            </div>
        </div>
    )
}

export default Timer;