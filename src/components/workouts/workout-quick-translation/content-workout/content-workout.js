import {useState, useEffect} from "react";

import ViewInitialStateLoading from "../../../loading/loading-initial-state/loading-initial-state";
import Error from "../../../error/error";

import "./content-workout.css"

const ContentWorkout = ({
                            error,
                            loading,
                            word,
                            changeAnswerList,
                            translation,
                            similarWords,
                            responseMessage,
                            setResponseMessage,
                            disabledButtons,
                            setDisabledButtons,
                            setCorrectAnswerBool,
                            changeCurrentWorkoutPage,
                            changeNumCorrectAnswers
}) => {
    const [seconds, setSeconds] = useState(1);
    const [timerActive, setTimerActive] = useState(false);
    const [pressBtnOnScreen, setPressBtnOnScreen] = useState(null);

    const [selectedWords, setSelectedWords] = useState(null);

    useEffect(() => {
        if (seconds > 0 && timerActive) {
            setTimeout(setSeconds, 100, seconds - 1);
        } else if (seconds === 0) {
            setDisabledButtons(false);
            setTimerActive(false);
            setPressBtnOnScreen(null);
            setResponseMessage("")
            changeCurrentWorkoutPage();
        }
    }, [seconds]);

    const changePressButtonOnScreen = (e) => {
        setPressBtnOnScreen(e.key);
    }

    useEffect(() => {
        window.addEventListener('keydown', (e) => changePressButtonOnScreen(e));
        return () => {
            window.removeEventListener('keydown', (e) => changePressButtonOnScreen(e));
        }
    }, [])

    useEffect(() => {
        if (pressBtnOnScreen === "ArrowLeft" && !disabledButtons){
             setSelectedWords(similarWords[0]);
        } else if (pressBtnOnScreen === "ArrowRight" && !disabledButtons){
             setSelectedWords(similarWords[1]);
        }
    }, [pressBtnOnScreen])

    useEffect(() => {
        if(selectedWords){
            showResults(selectedWords)
        }
    }, [selectedWords])

    const correctAnswer = selectedWords && selectedWords === translation;
    const incorrectAnswer = selectedWords && selectedWords !== translation;
    const correctAnswerLate =
        selectedWords && selectedWords === translation && responseMessage;
    const incorrectAnswerLate =
        selectedWords && selectedWords !== translation && responseMessage;

    const showResults = (chosenWord) => {
        if (correctAnswerLate){
                document.getElementById(chosenWord).className += " gray-green";
                changeAnswerList({"word": word, "is_true": 'false'})
        } else if (incorrectAnswerLate) {
                document.getElementById(chosenWord).className += " gray-red";
                changeAnswerList({"word": word, "is_true": 'false'})
        } else if (correctAnswer) {
                document.getElementById(chosenWord).className += " green";
                setResponseMessage("Верно!")
                changeNumCorrectAnswers();
                setCorrectAnswerBool(true);
                changeAnswerList({"word": word, "is_true": 'true'})
        } else if (incorrectAnswer) {
                document.getElementById(chosenWord).className += " red";
                setResponseMessage("Вы ошиблись")
                setCorrectAnswerBool(false);
                changeAnswerList({"word": word, "is_true": 'false'})

        }
        setDisabledButtons(true);
        setTimerActive(true);
        setSeconds(15);
        setPressBtnOnScreen(null);
    }

    const getElements = () => {
        return similarWords.map((item) => {
            return (
                <div key={item} className="col-md-5 col-xs-12 translations-q">
                    <button className="btn-modal-q"
                            id={item}
                            onClick={() => setSelectedWords(item)}
                            disabled={disabledButtons}>
                        {item}
                    </button>
                </div>
            )
        })
    }

    const loadingElement = loading ? <ViewInitialStateLoading/> : null;
    const errorElement = error ? <Error error={error}/>: null;

    const displayArrow = loading || error ? "hide" : null;

    return(
        <>
            <div className="content-workout-q">
                {loadingElement}
                {errorElement}
                <div className="row word-for-translation-q">
                        {word}
                </div>
                <div className="row">
                    <div id="ArrowLeft" className={`col-md-1 switch-arrows ${displayArrow}`}>←</div>
                    {getElements()}
                    <div id="ArrowRight" className={`col-md-1 switch-arrows ${displayArrow}`}>→</div>
                </div>
            </div>
            <div className="row result">{responseMessage}</div>
        </>
    )
}

export default ContentWorkout;