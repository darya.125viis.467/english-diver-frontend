import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'

import PrivateRoute from '../../helpers/private-route'
import Logo from "../logo/logo";
import WorkoutQuickTranslation
    from "../../pages/workout-quick-translation-page/workout-quick-translation-page";
import WorkoutWordConstructor
    from "../../pages/workout-word-constructor-page/workout-word-constructor-page";
import WorkoutWordTranslation
    from "../../pages/workout-word-translation-page/workout-word-translation-page";
import WorkoutMulti from "../../pages/workout-multi-page/workout-multi-page";
import {
    AuthorizationPage,
    CategoryListPage,
    WorkoutListPage,
    PasswordRecoveryEmailEntryPage,
    PasswordResetPage,
    RegistrationPage,
    CategoryPage,
    UserDictionaryPage
} from "../../pages";

import './App.css';
import ScrollToTop from "../../helpers/scroll-to-top";

const App = () => {
    return (
        <div className="preloading fadeIn">
            <Router>
                <ScrollToTop/>
                <Logo/>
                <Switch>
                    <Route exact path="/authorization">
                        <AuthorizationPage/>
                    </Route>

                    <Route exact path="/registration">
                        <RegistrationPage/>
                    </Route>

                    <Route exact path="/password-recovery">
                        <PasswordRecoveryEmailEntryPage/>
                    </Route>

                    <Route exact path="/api/user/password/reset/confirm">
                        <PasswordResetPage/>
                    </Route>

                    <PrivateRoute exact path="/">
                        <WorkoutListPage/>
                    </PrivateRoute>

                    <PrivateRoute exact path="/user-dictionary">
                        <UserDictionaryPage/>
                    </PrivateRoute>

                    <PrivateRoute exact path="/set-of-words/:id">
                        <CategoryPage/>
                    </PrivateRoute>

                    <PrivateRoute exact path="/word-sets">
                        <CategoryListPage/>
                    </PrivateRoute>

                    <PrivateRoute exact path="/word-translate">
                        <WorkoutWordTranslation/>
                    </PrivateRoute>

                    <PrivateRoute exact path="/fast-translate">
                        <WorkoutQuickTranslation/>
                    </PrivateRoute>

                    <DndProvider backend={HTML5Backend}>
                        <PrivateRoute exact path="/word-constructor">
                            <WorkoutWordConstructor/>
                        </PrivateRoute>

                        <PrivateRoute exact path="/multi-training">
                            <WorkoutMulti/>
                        </PrivateRoute>
                    </DndProvider>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
