import {useState, useEffect, useCallback} from "react";
import {Link} from "react-router-dom";
import Pagination from "react-js-pagination";
import debounce from "lodash.debounce";

import TrainingService from "../../../services/training-service";

import UserDictionaryWordsListItem from "../user-dictionary-words-list-item/user-dictionary-words-list-item";
import LoadingInitialState from "../../loading/loading-initial-state/loading-initial-state";
import LoadingPaginationAndSearch from "../../loading/loading-pagination-and-search/loading-pagination-and-search";

import "./user-dictionary-words-list.css";

const UserDictionaryWordsList = ({
                                     term,
                                     totalCountWords,
                                     setTotalCountWords,
                                     handleClick,
                                     isCheck,
                                     setIsCheck,
                                     listWords,
                                     setListWords,
                                     showButtonBar
}) => {
    const pageLimitCountWords = 18;

    const [initialStateLoading, setInitialStateLoading] = useState(true);
    const [loadingPaginationAndSearch, setLoadingPaginationAndSearch] = useState(false);
    const [showAlertElement,setShowAlertElement] = useState(false)

    const [currentPage, setCurrentPage] = useState(1);
    const [countWords, setCountWords] = useState(1);

    useEffect(() => {
        if(!initialStateLoading){
            setLoadingPaginationAndSearch(true);
        }
        getDictionaryWordsFromServer();
    }, [currentPage, term])

    useEffect(() => {
        setCurrentPage(1);
    }, [term])

    const getDictionaryWordsFromServer = useCallback(debounce(() =>{
        TrainingService.getDictionaryWords(pageLimitCountWords, getOffset(), term).then(res => {

            setIsCheck([]);

            setListWords(res.data.results);
            setCountWords(res.data.count);

            if (!term){
                setTotalCountWords(res.data.count);
                setShowAlertElement(!!!res.data.count);
            }

            if (initialStateLoading) {
                setInitialStateLoading(false);
            }
            setLoadingPaginationAndSearch(false);
        })
    }, 1000), [term, currentPage])

    const getOffset = () => {
        if(currentPage === 1){
            return 0;
        } else {
            return (currentPage - 1) * pageLimitCountWords;
        }
    }

    const onDeleteWord = (id) => {
        TrainingService.deleteWordFromDictionary(id);

        setListWords(listWords => listWords.filter(item => item.word.id !== id));
        setIsCheck(isCheck.filter(item => item !== id))

        setTotalCountWords(totalCountWords => totalCountWords - 1);
        setCountWords(countWords => countWords - 1);

        setShowAlertElement(!!!(totalCountWords - 1));
    }

    const onChangePagination = (currentPage) => {
        setCurrentPage(currentPage);
        setLoadingPaginationAndSearch(true);
    }

    const getElement = () =>  {
        return listWords.map(item => {
            return <UserDictionaryWordsListItem key={item.id}
                                                id={item.word.id}
                                                word={item.word}
                                                englishWord={item.word.english}
                                                russianWord={item.word.russian}
                                                onDeleteWord={onDeleteWord}
                                                handleClick={handleClick}
                                                isCheck={isCheck}
                                                showButtonBar={showButtonBar}/>
        })
    }

    const noWordsOnWorkoutElement = <div className="alert-element">Вы пока не добавили слова для изучения.
                                         Слова можно добавить на странице
                                         <Link to="/word-sets"> "Наборы слов"</Link>.
                                    </div>;

    const noMatchesInSearch = <div className="alert-element">Ничего не найдено.</div>;

    return (
        <div className="row">
            {loadingPaginationAndSearch ? <LoadingPaginationAndSearch/>: null}
            {initialStateLoading ? <LoadingInitialState/> : getElement()}
            {!countWords ? noMatchesInSearch : null}
            {showAlertElement ? noWordsOnWorkoutElement : null}
            {loadingPaginationAndSearch ? <LoadingPaginationAndSearch/>: null}

            {initialStateLoading || showAlertElement || !countWords ? null :
                <Pagination innerClass="pagination pagination-lg"
                            activePage={currentPage}
                            itemsCountPerPage={pageLimitCountWords}
                            totalItemsCount={countWords}
                            pageRangeDisplayed={5}
                            onChange={onChangePagination}/>}
        </div>
    )
}
export default UserDictionaryWordsList;
