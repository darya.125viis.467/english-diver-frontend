import basketImage from "../../../images/basket.png";

import './user-dictionary-words-list-item.css';

const UserDictionaryWordsListItem = ({
                           id,
                           englishWord,
                           russianWord,
                           onDeleteWord,
                           handleClick,
                           isCheck,
                           showButtonBar
}) => {

    return (
            <a className="list-group-item list-group-item-action">
                <div>
                     <img className="img-list-group-basket"
                          src={basketImage}
                          onClick={() => onDeleteWord(id)}/>
                </div>
                {showButtonBar &&
                <input className="input-checkbox"
                       type="checkbox"
                       name={englishWord}
                       checked={isCheck.includes(englishWord)}
                       onChange={handleClick}
                       />
                }
                {englishWord}
                <div className="translate-text">{russianWord}</div>
            </a>
        );
}

export default UserDictionaryWordsListItem;
