import { useEffect, useState} from "react";

import TrainingService from "../../../services/training-service";

import "./button-bar.css"

const ButtonBar = ({handleSelectAll, isCheck, isCheckAll, showButtonBar, toggleShowButtonBar}) => {

    const [workoutList, setWorkoutList] = useState([]);
    const [selectedWorkout, setSelectedWorkout] = useState(null);

    useEffect(() => {
        onSetWorkoutList();
    }, [])

    const onSetWorkoutList = () => {
        setWorkoutList([]);
        TrainingService.getTrainingTypes().then(res => {
            res.data.results.map(item =>
                setWorkoutList(workoutList => [...workoutList, { id: item.id,
                                                                 name: item.name,
                                                                 wordsCanBeChosen: item.words_can_be_chosen }]))
        })
    }

    const printWorkoutsData = () => {
        return workoutList.map(item => {
            if(item.wordsCanBeChosen){
                return <option key={item.id} value={item.id}>{item.name}</option>
            }
        })
    }

    const getSelectedWorkout = (e) => {
        setSelectedWorkout(e.target.value)
    }

    const sendWordsToWorkout = () => {
        if(isCheck.length && selectedWorkout){
            TrainingService.sendWordsToWorkout(isCheck, selectedWorkout).then(() =>
                alert("Слова успешно отправлены на тренировку")
            );
        } else {
            alert("Не выбраны слова или название тренировки");
        }
    }

    return (
        <div>
            {!showButtonBar &&
            <button type="button"
                    className="btn btn-primary button-b"
                    onClick={toggleShowButtonBar}>
                Отправить слова на тренировку
            </button>
            }
            {showButtonBar &&
            <>
                <div className="col-md-8 col-xs-8">
                    <button type="button"
                            className="btn btn-primary button-b"
                            onClick={toggleShowButtonBar}>Скрыть</button>
                    <select className="form-select button-b"
                            onChange={(e)=>getSelectedWorkout(e)}>
                        <option key="defaultValue" value="" selected>Выбрать тренировку</option>
                        {printWorkoutsData()}
                    </select>
                    <button type="button"
                            className="btn btn-primary button-b"
                            onClick={sendWordsToWorkout}>
                        Отправить на тренировку
                    </button>
                </div>
                <div className="col-md-4 col-xs-4 div-input-button-bar">
                    <input className="input-checkbox"
                           type="checkbox"
                           checked={isCheckAll}
                           onChange={() => handleSelectAll()}/>
                    Выбрать все слова
                </div>
            </>
            }
        </div>
    )
}

export default ButtonBar;