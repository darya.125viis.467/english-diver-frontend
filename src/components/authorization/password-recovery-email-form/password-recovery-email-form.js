import { Component } from "react";

import AuthService from "../../../services/auth-service";

import '../login-form/login-form.css';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import {isEmail} from "validator";
import {Link} from "react-router-dom";


const required = value => {
    if (!value) {
        return (
            <div className="error-text" role="alert">
                Заполните поле формы.
            </div>
        );
    }
};

const email = value => {
    if (!isEmail(value)) {
        return (
            <div className="error-text" role="alert">
                Введите верный формат e-mail.
            </div>
        );
    }
};

class PasswordRecoveryEmailForm extends Component {
    constructor(props) {
        super(props);
        this.handleReceiveLetter = this.handleReceiveLetter.bind(this);
        this.onChangeEmail= this.onChangeEmail.bind(this);

        this.state = {
            email: "",
            loading: false,
            message: ""
        };
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    handleReceiveLetter(e) {
        e.preventDefault();

        this.setState({
            message: "",
            loading: true
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            AuthService.sendEmailRecoverPassword(this.state.email).then(
                () => {
                    this.setState({
                        message: "Письмо успешно отправлено. Проверьте Вашу почту."
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        loading: false,
                        message: "Ошибка. К данной почте не привязан аккаунт."
                    });
                }
            );
        }
    }

    render() {
        return(
            <div className="container auth-modal-content">
                <Form onSubmit={this.handleReceiveLetter} ref={c => {this.form = c}}>
                    <div className="modal-title"> Введите e-mail, на который придет письмо для восстановления пароля</div>
                    <Input placeholder="E-mail"
                           name="name"
                           value={this.state.email}
                           onChange={this.onChangeEmail}
                           validations={[required, email]}
                           className="form-control modal-input"/>
                    <CheckButton className="btn btn-primary btn-modal"
                            disabled={this.state.loading}>
                        Восстановить пароль
                    </CheckButton>

                    {this.state.message && (
                        <div className="form-group">
                            <div className="error-text" role="alert">
                                {this.state.message}
                            </div>
                        </div>
                    )}

                    <CheckButton
                        style={{ display: "none" }}
                        ref={c => {
                            this.checkBtn = c;
                        }}
                    />

                    <Link to="/authorization" className="modal-link">Вернуться назад</Link>
                </Form>
            </div>
        )
    }
}

export default PasswordRecoveryEmailForm;