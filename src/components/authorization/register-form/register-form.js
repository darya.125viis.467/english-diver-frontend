import { Component } from "react";
import {Link, withRouter} from "react-router-dom";

import AuthService from '../../../services/auth-service';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import './register-form.css';

const required = value => {
    if (!value) {
        return (
            <div className="error-text" role="alert">
                Заполните поле формы!
            </div>
        );
    }
};

const email = value => {
    if (!isEmail(value)) {
        return (
            <div className="error-text" role="alert">
                Введите верный формат e-mail.
            </div>
        );
    }
};

const vpassword = value=> {
    if (value.length < 8 || value.length > 128) {
        return (
            <div className="error-text" role="alert">
                Пароль должен быть не меньше 8 символов.
            </div>
        );
    }
};


class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);
        this.onChangeUserName = this.onChangeUserName.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangePasswordCheck = this.onChangePasswordCheck.bind(this);

        this.state = {
            userName: "",
            email: "",
            password: "",
            passwordCheck: "",
            successful: false,
            message: ""
        };
    }

    onChangeUserName(e) {
        this.setState({
            userName: e.target.value
        });
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    onChangePasswordCheck(e) {
        this.setState({
            passwordCheck: e.target.value
        });
    }

    handleRegister(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0 &&
            this.state.password === this.state.passwordCheck) {
            AuthService.register(
                this.state.userName,
                this.state.email,
                this.state.password,
            ).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });

                    this.props.history.push("/authorization");
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: "Ошмбка. Аккаунт с такой почтой уже существует."
                    });
                }
            );
        }
    }

    render(){
        return(
            <div className="container reg-modal-content">
                    <Form onSubmit={this.handleRegister}
                          ref={c => {
                              this.form = c;
                          }}>
                        <div className="modal-title"> Зарегистрироваться на сайте</div>
                        <Input type="email"
                               placeholder="E-mail"
                               name="email"
                               value={this.state.email}
                               onChange={this.onChangeEmail}
                               validations={[required, email]}
                               className="form-control modal-input"/>
                        <Input type="userName"
                               placeholder="Ваш ник"
                               name="username"
                               value={this.state.userName}
                               onChange={this.onChangeUserName}
                               validations={[required]}
                               className="form-control modal-input"/>
                        <Input type="password"
                               placeholder="Пароль"
                               name="password"
                               value={this.state.password}
                               onChange={this.onChangePassword}
                               validations={[required, vpassword]}
                               className="form-control modal-input"/>
                        <Input type="password"
                               placeholder="Подтвердить пароль"
                               name="confirmPassword"
                               value={this.state.passwordCheck}
                               onChange={this.onChangePasswordCheck}
                               validations={[required, vpassword]}
                               className="form-control modal-input"/>

                        {this.state.password !== this.state.passwordCheck &&
                        this.checkBtn.context._errors.length === 0 &&(
                            <div className="error-text" role="alert">
                                Пароли не совпадают.
                            </div>
                        )}

                        <CheckButton className="btn btn-primary btn-modal">Создать аккаунт</CheckButton>

                        {this.state.message && (
                            <div className="form-group">
                                <div className="error-text" role="alert">
                                    {this.state.message}
                                </div>
                            </div>
                        )}

                        <CheckButton
                            style={{ display: "none" }}
                            ref={c => {
                                this.checkBtn = c;
                            }}
                        />
                    </Form>
                    <Link to="/authorization" className="modal-link">Войдите, если есть аккаунт</Link>
            </div>
        )
    }
}

export default withRouter(RegisterForm);