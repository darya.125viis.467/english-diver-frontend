import { Component } from "react";

import AuthService from "../../../services/auth-service";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import '../login-form/login-form.css';

const required = value => {
    if (!value) {
        return (
            <div className="error-text" role="alert">
                Заполните поле формы.
            </div>
        );
    }
};

const vpassword = value => {
    if (value.length < 8 || value.length > 128) {
        return (
            <div className="error-text" role="alert">
                Пароль должен быть не меньше 8 символов.
            </div>
        );
    }
};


class PasswordRecoveryForm extends Component {
    constructor(props) {
        super(props);
        this.handlePasswordRecovery = this.handlePasswordRecovery.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangePasswordCheck = this.onChangePasswordCheck.bind(this);

        this.params = new URLSearchParams(document.location.search.substring(1));
        this.token = this.params.get("token");
        this.id = this.params.get("uid");

        this.state = {
            password: "",
            passwordCheck: "",
            loading: false,
            message: ""
        };
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    onChangePasswordCheck(e) {
        this.setState({
            passwordCheck: e.target.value
        });
    }

    handlePasswordRecovery(e) {
        e.preventDefault();

        this.setState({
            message: "",
            loading: true
        });

        this.form.validateAll();

        console.log(this.id);
        console.log(this.token);

        if (this.checkBtn.context._errors.length === 0 &&
            this.state.password === this.state.passwordCheck) {
            AuthService.passwordRecovery(this.state.password,
                                         this.state.passwordCheck,
                                         this.id,
                                         this.token).then(
                () => {
                    this.setState({
                        message: "Пароль успешно изменён."
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        loading: false,
                        message: "Ошибка. Не удалось сменить пароль. Попробуйте еще раз."
                    });
                }
            );
        }
    }

    render() {
        return(
            <div className="container auth-modal-content">
                <Form onSubmit={this.handlePasswordRecovery} ref={c => {this.form = c}}>
                    <div className="modal-title"> Введите новый пароль для входа на сайт</div>
                    <Input type="password"
                           placeholder="Пароль"
                           name="password"
                           value={this.state.password}
                           onChange={this.onChangePassword}
                           validations={[required, vpassword]}
                           className="form-control modal-input"/>
                    <Input type="password"
                           placeholder="Подтвердить пароль"
                           name="confirmPassword"
                           value={this.state.passwordCheck}
                           onChange={this.onChangePasswordCheck}
                           validations={[required, vpassword]}
                           className="form-control modal-input"/>
                    <CheckButton className="btn btn-primary btn-modal"
                            disabled={this.state.loading}>
                        Поменять пароль
                    </CheckButton>

                    {this.state.message && (
                        <div className="form-group">
                            <div className="error-text" role="alert">
                                {this.state.message}
                            </div>
                        </div>
                    )}

                    <CheckButton
                        style={{ display: "none" }}
                        ref={c => {
                            this.checkBtn = c;
                        }}
                    />

                    {this.state.password !== this.state.passwordCheck &&
                    this.checkBtn.context._errors.length === 0 &&(
                        <div className="error-text" role="alert">
                            Ошибка. Пароли не совпадают.
                        </div>
                    )}
                </Form>
            </div>
        )
    }
}

export default PasswordRecoveryForm;