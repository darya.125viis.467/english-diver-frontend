import { Component } from "react";
import {Link, withRouter} from "react-router-dom";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import AuthService from '../../../services/auth-service';

import './login-form.css';

const required = value => {
    if (!value) {
        return (
            <div className="error-text" role="alert">
                Заполните поле формы.
            </div>
        );
    }
};

const email = value => {
    if (!isEmail(value)) {
        return (
            <div className="error-text" role="alert">
                Введите верный формат e-mail.
            </div>
        );
    }
};

const vpassword = value => {
    if (value.length < 8 || value.length > 128) {
        return (
            <div className="error-text" role="alert">
                Пароль должен быть не меньше 8 символов.
            </div>
        );
    }
};


class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
        this.onChangeEmail= this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);

        this.state = {
            email: "",
            password: "",
            loading: false,
            message: ""
        };
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    handleLogin(e) {
        e.preventDefault();

        this.setState({
            message: "",
            loading: true
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            AuthService.login(this.state.email, this.state.password).then(
                () => {
                    this.props.history.push("/");
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        loading: false,
                        message: "Ошибка входа. Проверьте правильность введенных данных."
                    });
                }
            );
        }
    }

    render(){
        return(
            <div className="container-fluid auth-modal-content">
                <Form onSubmit={this.handleLogin} ref={c => {this.form = c}}>
                    <div className="modal-title"> Войди на сайт и начинай обучение</div>
                    <Input placeholder="E-mail"
                           name="name"
                           value={this.state.email}
                           onChange={this.onChangeEmail}
                           validations={[required, email]}
                           className="form-control modal-input"/>
                    <Input type="password"
                           placeholder="Пароль"
                           name="password"
                           value={this.state.password}
                           onChange={this.onChangePassword}
                           validations={[required, vpassword]}
                           className="form-control modal-input"/>
                    <CheckButton className="btn btn-primary btn-modal"
                                 disabled={this.state.loading}>
                        Вход
                    </CheckButton>

                    {this.state.message && (
                        <div className="form-group">
                            <div className="error-text" role="alert">
                                {this.state.message}
                            </div>
                        </div>
                    )}

                    <CheckButton
                        style={{ display: "none" }}
                        ref={c => {
                            this.checkBtn = c;
                        }}
                    />

                    <Link to="/password-recovery" className="modal-link">Забыли пароль?</Link>
                    <Link to="/registration" className="modal-link">Создать аккаунт</Link>
                </Form>
            </div>
        )
    }
}

export default withRouter(LoginForm);

