import './login-header.css'

const LoginHeader = () => {

    return(
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div className="works-hero-bg-autho">
                        <h2> EnglishDiver - твой лучший помощник в изучении английского </h2>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginHeader;