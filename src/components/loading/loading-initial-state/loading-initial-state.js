import "./loading-initial-state.css";

const LoadingInitialState = () => {

    const getLoadingElements = () => {
        const arrDivFountainG = [];
        const countDivFountainG = 8;

        for (let i = 0; i < countDivFountainG ; i++){
            arrDivFountainG.push(<div key={i} id={`fountainG_${i}`} className="fountainG"/>);
        }
        return arrDivFountainG;
    }

    return (
        <div id="fountainG">
            {getLoadingElements()}
        </div>
    )
}

export default LoadingInitialState;