import "./loading-pagination-and-search.css";

const LoadingPaginationAndSearch = () => {
    return (
        <div className="progress" style={{ 'margin-bottom': 0, 'height': 5 }}>
            <div className="progress-bar progress-bar-striped active"
                 role="progressbar"
                 aria-valuenow="75"
                 aria-valuemin="0"
                 aria-valuemax="100"
                 style={{
                     width: '100%',
                     'background-color': 'rgba(105,144,187,0.87)'}}/>
        </div>
    )
}

export default LoadingPaginationAndSearch;