import {useState} from "react";

import TrainingService from "../../../services/training-service";

import plusImage from "../../../images/plus.png";
import okImage from "../../../images/ok.png";

import './category-list-item.css';

const CategoryListItem = ({englishWord, russianWord, isLinked}) =>{

    const [added, setAdded] = useState(isLinked);

    const onToggleAdded = () => {
        setAdded(added => !added);
        if (!added){
            TrainingService.addWordInDictionary(englishWord);
        } else {
            TrainingService.deleteWordFromDictionary(englishWord);
        }
    }

    const hintTitle = added ? "Удилить из личного словаря" : "Добавить в личный словарь";

    return (
        <a className="list-group-item list-group-item-action">
            <div>
                <img className="img-list-group-add"
                     src={added ? okImage : plusImage}
                     alt="Добавить/удалить слово из личного словаря"
                     onClick={onToggleAdded}
                     data-bs-toggle="tooltip"
                     title={hintTitle}/>
            </div>
            {englishWord}
            <div className="translate-text">{russianWord}</div>
        </a>
    )
}

export default CategoryListItem;