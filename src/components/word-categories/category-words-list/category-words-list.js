import {useState, useEffect, useCallback} from "react";
import Pagination from "react-js-pagination";
import debounce from "lodash.debounce";

import CategoryListItem from "../category-list-item/category-list-item";
import TrainingService from "../../../services/training-service";
import ViewInitialStateLoading from "../../loading/loading-initial-state/loading-initial-state";
import LoadingPaginationAndSearch from "../../loading/loading-pagination-and-search/loading-pagination-and-search";


const CategoryWordsList = ({term, categoryId}) => {

    const pageLimitCountWords = 18;

    const [initialStateLoading, setInitialStateLoading] = useState(true);
    const [loadingPaginationAndSearch, setLoadingPaginationAndSearch] = useState(false);

    const [listWords, setListWords] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [countWords, setCountWords] = useState(0);

    useEffect(() => {
        if(!initialStateLoading){
            setLoadingPaginationAndSearch(true);
        }
        getCategoryWordsFromServer();
    }, [currentPage, term])


    const getOffset = () => {
        if(currentPage === 1){
            return 0;
        } else {
            return (currentPage - 1) * pageLimitCountWords;
        }
    }

    const getCategoryWordsFromServer = useCallback(debounce(() => {
        TrainingService.getCategoryWords(categoryId, pageLimitCountWords, getOffset(), term).then(res => {
            const listWords = res.data.results;

            setListWords(listWords);
            setCountWords(res.data.count);

            if (initialStateLoading) {
                setInitialStateLoading(false);
            }
            setLoadingPaginationAndSearch(false);
        })
    }, 1000), [term, currentPage])

    const onChangePagination = (currentPage) => {
        setCurrentPage(currentPage);
    }

    const getElement = () => {
        return listWords.map(item => {
            return <CategoryListItem key={item.id}
                                     englishWord={item.english}
                                     russianWord={item.russian}
                                     isLinked={item.is_linked}/>
        })
    }

    return (
        <div className="row">
            {loadingPaginationAndSearch ? <LoadingPaginationAndSearch/>: null}
            {initialStateLoading ? <ViewInitialStateLoading/> : getElement()}
            {loadingPaginationAndSearch ? <LoadingPaginationAndSearch/>: null}

            {initialStateLoading ? null : <Pagination innerClass="pagination pagination-lg"
                                                      activePage={currentPage}
                                                      itemsCountPerPage={pageLimitCountWords}
                                                      totalItemsCount={countWords}
                                                      pageRangeDisplayed={5}
                                                      onChange={onChangePagination}/>
            }
        </div>
    )
}
export default CategoryWordsList;
