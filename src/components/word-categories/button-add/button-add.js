import TrainingService from "../../../services/training-service";

import "./button-add.css";

const ButtonAdd = ({categoryId}) => {

    const addAllWordsToUserDictionary = () => {
        TrainingService.addAllWordsInDictionary(categoryId).then(() => {
            alert("Категория успешно добавлена в личный словарь.");
        })
    }

    return(
        <div className="col align-self-end ">
            <button type="button"
                    className="btn btn-primary button-add"
                    onClick={addAllWordsToUserDictionary}>
                Добавить набор
            </button>
        </div>
    )
}

export default ButtonAdd;