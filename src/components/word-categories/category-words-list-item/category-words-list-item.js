import { Link } from "react-router-dom";

import './category-words-list-item.css';

const CategoryWordsListItem = ({heading, wordsCount, id, image}) =>{

    return (
        <div className="col-md-4 col-xs-12 col-dict">
            <div className="prjct-bg-dict">
                <Link to={`/set-of-words/${id}`} >
                    <img src={image} alt={heading} className="img-prjct-wrk"/>
                </Link>
                <div className="prjct-wrt-left-wrk"/>
                <div className="shw-cs2-dict"> {heading} </div>
                <div className="shw-cs"> Количество слов: {wordsCount} </div>
            </div>
        </div>
    )

}

export default CategoryWordsListItem;