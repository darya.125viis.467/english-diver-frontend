import CategoryWordsListItem from "../category-words-list-item/category-words-list-item";

const CategoryList = ({itemsList}) => {

    const getElement = () => {
        return itemsList.map((item) => {
            return  <CategoryWordsListItem key={item.id}
                                           heading={item.name}
                                           wordsCount={item.words_count}
                                           id={item.id}
                                           image={item.image}/>
        });
    }

    return (
        <div className="row">
            {getElement()}
        </div>
    );
}

export default CategoryList;