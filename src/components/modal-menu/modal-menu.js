import { useState, useEffect } from "react";
import {NavLink} from "react-router-dom";

import AuthService from "../../services/auth-service";

import './modal-menu.css';

const ModalMenu = ({active, setActive}) => {

    const [name, setName] = useState(null);

    useEffect(() => {
        getUserName();
    }, [])

    const getUserName = () => {
        AuthService.getCurrentUserName().then(res => {
            const name = res.data.username;
            setName(name);
        })
    }

    const setActiveLink = isActive => (!isActive ? " menu-links" : " disabled-menu-links");

    return (
        <div className="right-menu">

            <button type="button" className="btn btn-primary-menu" data-toggle="modal"
                    data-target="#exampleModalLong" onClick={setActive}>
                Меню
            </button>

            <div className={active ? "my-modal active" : "my-modal"}
                 id="exampleModalLong"
                 tabIndex="-1"
                 role="dialog"
                 aria-labelledby="exampleModalLongTitle"
                 aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className={active ? "my-modal-content active" : "my-modal-content"}>
                        <div className="modal-header-menu">
                            <div className="menu-t">
                                <div className="open-menu-name">
                                    Привет, {name}!
                                </div>

                                <div className="open-menu-title">
                                    EnglishDiver
                                </div>
                                <div>
                                    <NavLink className={setActiveLink}
                                             exact to="/"> Тренировки </NavLink>
                                </div>
                                <div>
                                    <NavLink className={setActiveLink}
                                             exact to="/word-sets"> Наборы слов </NavLink>
                                </div>
                                <div>
                                    <NavLink className={setActiveLink}
                                             exact to="/user-dictionary"> Личный словарь </NavLink>
                                </div>
                                <div>
                                    <NavLink className="menu-links"
                                             exact to="/authorization"
                                             onClick={() => AuthService.logout()}> Выход </NavLink>
                                </div>
                                <button type="button" className="menu-close" data-dismiss="modal"
                                        aria-label="Close" onClick={setActive}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default ModalMenu;