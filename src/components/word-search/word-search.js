import './word-search.css';

const WordSearch = ({term, setTerm}) => {

    const onUpdateTerm = (e) => {
        setTerm(e.target.value);
    }

    const onUpdateSearch = (e) => {
        e.preventDefault();
        setTerm(term);
    }

    return (
        <form onSubmit={onUpdateSearch}>
            <div className="col-md-4 col-xs-4 col-search">
                <input type="text"
                       className="form-control input-search"
                       id="inputSearch"
                       name="inputSearch"
                       placeholder="Поиск..."
                       aria-label="Поиск"
                       value={term}
                       onChange={onUpdateTerm}/>
            </div>
        </form>
    )
}

export default WordSearch;
