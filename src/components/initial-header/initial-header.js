import './initial-header.css';

const InitialHeader = ({firstHeading, secondHeading}) => {

    return (
        <div className="row">
            <div className="col-md-12">
                <div className="works-hero-bg">
                    <h1 className="fade-anime-top"> {firstHeading} </h1>
                    <h2 className="fade-anime-bottom"> {secondHeading} </h2>
                </div>
            </div>
        </div>
    )
}

export default InitialHeader;