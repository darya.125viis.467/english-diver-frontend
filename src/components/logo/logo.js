import {Link} from "react-router-dom";

import logoDiver from "../../images/logodiver.png";

import './logo.css';

const Logo = () => {
    return(
        <Link to="/" className="nav-ttl">
            <img src={logoDiver}
                 alt="logo EnglishDiver"
                 width="30"
                 height="30"
                 className="d-inline-block align-text-top"/>
            EnglishDiver
        </Link>
    )
}

export default Logo;