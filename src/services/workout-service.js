import axiosApiInstance from "./interceptors"

const API_URL = "https://english-diver.herokuapp.com/api/training/";

class WorkoutService {

    getDataForWorkout(id){
        return axiosApiInstance.post(API_URL + "training-types/" + id + "/start/")
    }

    endOfWorkout(id, answerList){
        return axiosApiInstance.post(API_URL + "training-types/" + id + "/finish/", {'result': answerList})
    }


}

export default new WorkoutService();

