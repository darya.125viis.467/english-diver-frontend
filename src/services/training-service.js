import axiosApiInstance from "./interceptors"

const API_URL = "https://english-diver.herokuapp.com/api/training/";

class TrainingService {

    getListCategories(){
        return axiosApiInstance.get(API_URL + "categories/")
    }

    getCategory(id){
        return axiosApiInstance.get(API_URL + "categories/" + id)
    }

    getCategoryWords(categoryId, limit, offset, term){
        return axiosApiInstance.get(API_URL + "words/", {
            params: {
                'category': categoryId,
                'limit': limit,
                'offset': offset,
                'search': term,
            }
        })
    }

    getDictionaryWords(limit, offset, searchTerm){
        return axiosApiInstance.get(API_URL + "dictionary/", {
            params: {
                'limit': limit,
                'offset': offset,
                'search': searchTerm,
            }
        })
    }

    addWordInDictionary(word) {
        return axiosApiInstance.post(API_URL + "dictionary/", {'word': word})
    }

    addAllWordsInDictionary(categoryId){
        return axiosApiInstance.post(API_URL + "dictionary/add-category-words/",
            {'category_id': categoryId})
    }

    deleteWordFromDictionary(word){
        return axiosApiInstance.post(API_URL + "dictionary/remove-from-dictionary/", {'word': word})
    }

    searchWords(categoryId, limit, offset, term) {
        return axiosApiInstance.get(API_URL + "words/", {
            params: {
                'category': categoryId,
                'limit': limit,
                'offset': offset,
                'search': term
            }
        })
    }

    sendWordsToWorkout(listWordsForWorkout, trainingType) {
        return axiosApiInstance.post(API_URL + "dictionary/add-words-to-training/",
            {'words': listWordsForWorkout, 'training_type': trainingType})
    }

    getTrainingTypes(limit, offset, term) {
        return axiosApiInstance.get(API_URL + "training-types/", {
            params: {
                'limit': limit,
                'offset': offset,
                'search': term
            }
        })
    }



}

export default new TrainingService();