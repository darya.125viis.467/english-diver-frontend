import AuthService from "./auth-service";
import axios from "axios";

const axiosApiInstance = axios.create();

// Request interceptor for API calls
axiosApiInstance.interceptors.request.use(
    async config => {

        const accessToken = AuthService.getCurrentUserToken().access;
        config.headers = {
            'Authorization': `Bearer ${accessToken}`,
            'Accept': 'application/json',
        }
        return config;
    },
    error => {
        Promise.reject(error)
    });

// Response interceptor for API calls
axiosApiInstance.interceptors.response.use((response) => {
    return response
}, async function (error) {
    console.log(error)
    const originalRequest = error.config;
    if (error.response.status === 403 && !originalRequest._retry) {
        originalRequest._retry = true;
        const access_token = await AuthService.refreshAccessToken();
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
        return axiosApiInstance(originalRequest);
    }
    if(error.response.status === 401){
        AuthService.logout();
        window.location.href = '/authorization';
    }
    return Promise.reject(error);
});

export default axiosApiInstance;