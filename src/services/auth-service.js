import axios from "axios";
import axiosApiInstance from "./interceptors";

const API_URL = "https://english-diver.herokuapp.com/api/user/";

class AuthService {
    login(email, password) {
        return axios
            .post(API_URL + "token/", {
                "email": email,
                "password": password
            })
            .then(response => {
                if (response.data.access) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                    localStorage.setItem("email", JSON.stringify(email));
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem("user");
        localStorage.removeItem("email");
    }

    register(username, email, password) {
        return axios.post(API_URL + "registration/", {
            username,
            email,
            password
        });
    }

    sendEmailRecoverPassword(email) {
        return axios.post(API_URL + "password/reset/", {
            email,
        });
    }

    passwordRecovery(passwordOne, passwordTwo, id, token) {
        return axios.post(API_URL + "password/reset/confirm/", {
            "new_password1": passwordOne,
            "new_password2": passwordTwo,
            "uid": id,
            "token": token
        });
    }

    refreshAccessToken() {
        const refreshToken = this.getCurrentUserToken().refresh;

        return axios.post(API_URL + "token/refresh/", {
            "refresh": refreshToken
        })

    }

    async getCurrentUserName() {
        const res = await axiosApiInstance.get(API_URL + "user-info/" );
        return res;
    }


    getCurrentUserToken() {
        return JSON.parse(localStorage.getItem('user'))
    }

    getCurrentUserEmail() {
        return JSON.parse(localStorage.getItem('email'))
    }

}

export default new AuthService();