import {useEffect, useState} from "react";

import ModalMenu from "../components/modal-menu/modal-menu";
import CategoryWordsList from "../components/word-categories/category-words-list/category-words-list";
import NavigationButtons from "../components/navigation-buttons/navigation-buttons";
import Footer from "../components/footers/footer/footer";
import InitialHeader from "../components/initial-header/initial-header";
import WordSearch from "../components/word-search/word-search";
import ButtonAdd from "../components/word-categories/button-add/button-add";
import {withRouter} from "react-router-dom";
import TrainingService from "../services/training-service";

const CategoryPage = ({match}) => {

    const categoryId = match.params.id;

    const [totalCountWords, setTotalCountWords] = useState(0);
    const [nameCategory, setNameCategory] = useState('');
    const [term, setTerm] = useState('');

    const [modalActive, setModalActive] = useState(false);

    useEffect(() => {
        getCategoryNameAndTotalCountWords();
    }, [])

    const getCategoryNameAndTotalCountWords = () => {
        TrainingService.getCategory(categoryId).then(res => {
            const name = res.data.name;
            const totalCountWords = res.data.words_count;

            setNameCategory(name);
            setTotalCountWords(totalCountWords);
        })
    }

    const toggleModalActive = () => {
        setModalActive(modalActive => !modalActive);
    }

    const countWordsHeading = "Количество слов: " + totalCountWords;

    return (
        <>
            <ModalMenu active={modalActive}
                       setActive={toggleModalActive}/>
            <div className="container">
                <InitialHeader firstHeading={nameCategory}
                               secondHeading={countWordsHeading}/>
                <div className="row" style={{"margin-bottom": "25px"}}>
                    <WordSearch term={term}
                                setTerm={setTerm}/>
                    <ButtonAdd categoryId={categoryId}/>
                </div>
                <CategoryWordsList term={term}
                                   categoryId={categoryId}/>
                <NavigationButtons firstButtonName="Наборы слов"
                                   firstLink="/word-sets"
                                   secondButtonName="Тренировки"
                                   secondLink="/"/>
                <Footer/>
            </div>
        </>
    )
}

export default withRouter(CategoryPage);