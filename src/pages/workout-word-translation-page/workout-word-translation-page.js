import {useEffect} from "react";
import {Link} from "react-router-dom";

import useHttpWorkout from "../../hooks/useHttpWorkout"
import useWorkout from "../../hooks/useWorkout";

import WorkoutHeader from "../../components/workouts/workout-header/workout-header";
import WorkoutResult from "../../components/workouts/workout-result/workout-result";
import ContentWorkout from "../../components/workouts/workout-word-translation/content-workout/content-workout";

import './workout-word-translation-page.css';

const WorkoutWordTranslation = () => {

    const totalWorkoutPage = 5;

    const {
        dataForWorkout,
        changeAnswerList,
        sendResultsToServer,
        loading,
        error
    } = useHttpWorkout("word-translate");

    const {
        word,
        translation,
        similarWords,
        currentWorkoutPage,
        numCorrectAnswers,
        changeCurrentWorkoutPage,
        changeNumCorrectAnswers,
        getDataForWorkoutPage,
    } = useWorkout(dataForWorkout);

    useEffect(() => {
        if (dataForWorkout && currentWorkoutPage <= totalWorkoutPage) {
            getDataForWorkoutPage();
        } else if (currentWorkoutPage > totalWorkoutPage){
            sendResultsToServer();
        }
    }, [currentWorkoutPage, dataForWorkout])

    return(
        <div className="preloading-workout">
            <Link to="/"><div className="right-close">Выйти</div></Link>
            <div className="container">
                {currentWorkoutPage > totalWorkoutPage ?
                    <WorkoutResult totalWorkoutPage={totalWorkoutPage}
                                   numCorrectAnswers={numCorrectAnswers}
                                   styleContent={"content-workout"}
                                   styleButton={"btn-modal"}/> :
                    <>
                        <WorkoutHeader loading={loading}
                                       header={"Слово-перевод"}
                                       totalNumber={totalWorkoutPage}
                                       number={currentWorkoutPage}
                                       styleHeader={"works-hero"}/>
                        <div className="row content-workout">
                            <ContentWorkout error={error}
                                            loading={loading}
                                            russianWord={word}
                                            words={similarWords}
                                            key={currentWorkoutPage}
                                            translation={translation}
                                            changeAnswerList={changeAnswerList}
                                            changeCurrentWorkoutPage={changeCurrentWorkoutPage}
                                            changeNumCorrectAnswers={changeNumCorrectAnswers}/>
                        </div>
                    </>
                }
            </div>
        </div>
    )
}

export default WorkoutWordTranslation;