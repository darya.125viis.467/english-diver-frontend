import LoginHeader from "../components/authorization/login-header/login-header";
import LoginForm from "../components/authorization/login-form/login-form";
import AuthFooter from "../components/footers/auth-footer/auth-footer";

const AuthorizationPage = () => {
    return (
        <div className="backg">
            <LoginHeader/>
            <LoginForm/>
            <AuthFooter/>
        </div>
    )
}

export default AuthorizationPage;