import {useMemo, useState} from "react";

import TrainingService from "../services/training-service";

import withItemsList from "../hoc/with-items-list";
import ModalMenu from "../components/modal-menu/modal-menu";
import InitialHeader from "../components/initial-header/initial-header";
import NavigationButtons from "../components/navigation-buttons/navigation-buttons";
import Footer from "../components/footers/footer/footer";
import CategoryList from "../components/word-categories/category-list/category-list";

const CategoryListPage = () => {

    const CategoryListWithItemsList = useMemo(
        ()=> withItemsList(
            CategoryList,
            () => TrainingService.getListCategories()
    ), [])

    const [modalActive, setModalActive] = useState(false);

    const toggleModalActive = () => {
        setModalActive(modalActive => !modalActive)
    }

    return (
        <>
            <ModalMenu active={modalActive}
                       setActive={toggleModalActive}/>
            <div className="container">
                <InitialHeader firstHeading="Выбирай наборы слов для изучения"
                               secondHeading="Постоянно добавляются новые наборы слов. Не пропусти обновление!"/>
                <CategoryListWithItemsList/>
                <NavigationButtons firstButtonName="Тренировки"
                                   firstLink="/"
                                   secondButtonName="Мои слова"
                                   secondLink="/user-dictionary"/>
                <Footer/>
            </div>
        </>
    )
}

export default CategoryListPage;