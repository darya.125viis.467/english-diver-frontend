import LoginHeader from "../components/authorization/login-header/login-header";
import PasswordRecoveryEmailForm from "../components/authorization/password-recovery-email-form/password-recovery-email-form";
import AuthFooter from "../components/footers/auth-footer/auth-footer";

const PasswordRecoveryEmailEntryPage = () => {
    return (
        <div className="backg">
            <LoginHeader/>
            <PasswordRecoveryEmailForm/>
            <AuthFooter/>
        </div>
    )
}

export default PasswordRecoveryEmailEntryPage;