import {useMemo, useState} from "react";

import TrainingService from "../services/training-service";

import ModalMenu from "../components/modal-menu/modal-menu";
import InitialHeader from "../components/initial-header/initial-header";
import WorkoutList from "../components/workouts/workout-list/workout-list";
import NavigationButtons from "../components/navigation-buttons/navigation-buttons";
import Footer from "../components/footers/footer/footer";
import withItemsList from "../hoc/with-items-list";

const WorkoutListPage = () => {

    const WorkoutListWithItemsList = useMemo(
        ()=> withItemsList(
            WorkoutList,
            () => TrainingService.getTrainingTypes()
        ), [])

    const [modalActive, setModalActive] = useState(false);

    const toggleModalActive = () => {
        setModalActive(modalActive => !modalActive)
    }

    return (
        <>
            <ModalMenu active={modalActive}
                       setActive={toggleModalActive}/>
            <div className="container">
                <InitialHeader firstHeading="Учи слова быстро и эффективно!"
                               secondHeading="Выбери подходящую тренировку и начинай обучение"/>
                <WorkoutListWithItemsList/>
                <NavigationButtons firstButtonName="Наборы слов"
                                   firstLink="/word-sets"
                                   secondButtonName="Мои слова"
                                   secondLink="/user-dictionary"/>
                <Footer/>
            </div>
        </>
    )
}

export default WorkoutListPage;