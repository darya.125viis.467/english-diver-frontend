import LoginHeader from "../components/authorization/login-header/login-header";
import PasswordRecoveryForm from "../components/authorization/password-recovery-form/password-recovery-form";
import AuthFooter from "../components/footers/auth-footer/auth-footer";

const PasswordResetPage = () => {
    return (
        <div className="backg">
            <LoginHeader/>
            <PasswordRecoveryForm/>
            <AuthFooter/>
        </div>
    )
}

export default PasswordResetPage;