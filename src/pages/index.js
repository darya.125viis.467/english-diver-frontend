import AuthorizationPage from "./authorization-page";
import CategoryListPage from "./category-list-page";
import WorkoutListPage from "./workout-list-page";
import PasswordRecoveryEmailEntryPage from "./password-recovery-email-entry-page";
import PasswordResetPage from "./password-reset-page";
import RegistrationPage from "./registration-page";
import CategoryPage from "./category-page";
import UserDictionaryPage from "./user-dictionary-page";

export {
    AuthorizationPage,
    CategoryListPage,
    WorkoutListPage,
    PasswordRecoveryEmailEntryPage,
    PasswordResetPage,
    RegistrationPage,
    CategoryPage,
    UserDictionaryPage
}