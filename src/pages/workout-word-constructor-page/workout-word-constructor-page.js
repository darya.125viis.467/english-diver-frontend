import {useEffect} from "react";

import ContentWorkout from "../../components/workouts/workout-word-constructor/content-workout/content-workout";

import useHttpWorkout from "../../hooks/useHttpWorkout"
import useWorkout from "../../hooks/useWorkout";

import "./workout-word-constructor-page.css"
import WorkoutHeader from "../../components/workouts/workout-header/workout-header";
import WorkoutResult from "../../components/workouts/workout-result/workout-result";
import {Link} from "react-router-dom";

const WorkoutWordConstructorPage = () => {

    const totalWorkoutPage = 5;

    const {
        dataForWorkout,
        changeAnswerList,
        sendResultsToServer,
        loading,
        error
    } = useHttpWorkout("word-constructor");

    const {
        word,
        translation,
        currentWorkoutPage,
        numCorrectAnswers,
        jumbledTranslation,
        changeCurrentWorkoutPage,
        changeNumCorrectAnswers,
        getDataForWorkoutPage
    } = useWorkout(dataForWorkout);

    useEffect(() => {
        if (dataForWorkout && currentWorkoutPage <= totalWorkoutPage) {
            getDataForWorkoutPage();
        } else if (currentWorkoutPage > totalWorkoutPage){
        sendResultsToServer();
        }
    }, [currentWorkoutPage, dataForWorkout])

    return(
        <div className="preloading-workout-constructor">
            <Link to="/"><div className="right-close">Выйти</div></Link>
            <div className="container">
                { currentWorkoutPage > totalWorkoutPage  ?
                    <WorkoutResult totalWorkoutPage={totalWorkoutPage}
                                   numCorrectAnswers={numCorrectAnswers}
                                   styleContent={"content-workout-constructor"}
                                   styleButton={"btn-modal-constructor"}/> :
                    <>
                        <WorkoutHeader loading={loading}
                                       header={"Конструктор слов"}
                                       totalNumber={totalWorkoutPage}
                                       number={currentWorkoutPage}
                                       styleHeader={"works-hero"}/>
                        <div className="content-workout-constructor">
                            <ContentWorkout error={error}
                                            loading={loading}
                                            translation={translation}
                                            russianWord={word}
                                            jumbledTranslation={jumbledTranslation}
                                            changeAnswerList={changeAnswerList}
                                            changeNumCorrectAnswers={changeNumCorrectAnswers}
                                            changeCurrentWorkoutPage={changeCurrentWorkoutPage}/>
                        </div>
                    </>
                }
            </div>
        </div>
    )
}

export default WorkoutWordConstructorPage;

