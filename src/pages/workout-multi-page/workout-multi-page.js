import {useState, useEffect} from "react";
import {Link} from "react-router-dom";

import ContentWorkoutWordTranslation from
        "../../components/workouts/workout-word-translation/content-workout/content-workout";
import ContentWorkoutWordConstructor from
        "../../components/workouts/workout-word-constructor/content-workout/content-workout";
import ContentWorkout from "../../components/workouts/workout-multi/content-workout/content-workout";
import WorkoutHeader from "../../components/workouts/workout-header/workout-header";
import WorkoutResult from "../../components/workouts/workout-result/workout-result";

import useHttpWorkout from "../../hooks/useHttpWorkout";
import useWorkout from "../../hooks/useWorkout";

import "./workout-multi-page.css";

const WorkoutMultiPage = () => {

    const totalWorkoutPage = 15;

    const [currentTotalWorkoutPage, setCurrentTotalWorkoutPage] = useState(1);
    const [showTranslation, setShowTranslation] = useState(false);

    const {
        dataForWorkout,
        setAnswerList,
        answerList,
        changeAnswerList,
        sendResultsToServer,
        loading,
        error
    } = useHttpWorkout("multi-training");

    const {
        word,
        translation,
        similarWords,
        currentWorkoutPage,
        numCorrectAnswers,
        jumbledTranslation,
        setCurrentWorkoutPage,
        changeNumCorrectAnswers,
        getDataForWorkoutPage
    } = useWorkout(dataForWorkout);

    const onToggleShowTranslation = () => {
        setShowTranslation(showTranslation => !showTranslation);
    }

    useEffect(() => {
        if (dataForWorkout && currentWorkoutPage <= 5) {
            getDataForWorkoutPage();
        }  else {
            setCurrentWorkoutPage(1);
        }
    }, [currentWorkoutPage, dataForWorkout])

    useEffect(() => {
        if (currentTotalWorkoutPage > 15){
            setAnswerList(processingAnswerListToSend());
            sendResultsToServer();
        }
    }, [currentTotalWorkoutPage])


    const changeCurrentWorkoutPage = () => {
        setCurrentWorkoutPage(currentWorkoutPage => ++currentWorkoutPage)
        setCurrentTotalWorkoutPage(currentTotalWorkoutPage => ++currentTotalWorkoutPage)
    }

    const processingAnswerListToSend =  async () => {
        const translationWords = answerList.slice(0, 5);
        const constructorWords = answerList.slice(5, 10);
        const multiWords = answerList.slice(10, 15);

        const processedAnswerList = [];

        for (let i = 0; i < 5; i++){
            if (translationWords[i].is_true && constructorWords[i].is_true && multiWords[i].is_true){
                processedAnswerList.push({"word": translationWords[i].word, "is_true": true});
            } else {
                processedAnswerList.push({"word": translationWords[i].word, "is_true": false});
            }
        }
       return  processedAnswerList;
    }

    const getContentWorkout = () => {
        if (currentTotalWorkoutPage <= 5){
            return  <>
                <ContentWorkoutWordTranslation error={error}
                                               loading={loading}
                                               russianWord={word}
                                               showTranslation={showTranslation}
                                               words={similarWords}
                                               key={currentWorkoutPage}
                                               translation={translation}
                                               changeAnswerList={changeAnswerList}
                                               onToggleShowTranslation={()=>onToggleShowTranslation()}
                                               changeCurrentWorkoutPage={()=>changeCurrentWorkoutPage()}
                                               changeNumCorrectAnswers={()=>changeNumCorrectAnswers()}/>
            </>

        } else if (currentTotalWorkoutPage > 5 && currentTotalWorkoutPage <= 10){
            return <ContentWorkoutWordConstructor translation={translation}
                                                  russianWord={word}
                                                  jumbledTranslation={jumbledTranslation}
                                                  changeAnswerList={changeAnswerList}
                                                  changeNumCorrectAnswers={()=>changeNumCorrectAnswers()}
                                                  changeCurrentWorkoutPage={()=>changeCurrentWorkoutPage()}/>
        } else if (currentTotalWorkoutPage > 10 && currentTotalWorkoutPage <= 15){
            return <ContentWorkout russianWord={word}
                                   translation={translation}
                                   changeAnswerList={changeAnswerList}
                                   changeCurrentWorkoutPage={()=>changeCurrentWorkoutPage()}/>
        }
    }

    return(
        <div className="preloading-workout-multi">
            <Link to="/"><div className="right-close">Выйти</div></Link>
            <div className="container">
                { currentTotalWorkoutPage > totalWorkoutPage ?
                    <WorkoutResult totalWorkoutPage={totalWorkoutPage}
                                   numCorrectAnswers={numCorrectAnswers}
                                   styleContent={"content-workout"}
                                   styleButton={"btn-modal"}/> :
                    <>
                        <WorkoutHeader loading={loading}
                                       header={"Мульти-тренировка"}
                                       totalNumber={totalWorkoutPage}
                                       number={currentTotalWorkoutPage}
                                       styleHeader={"works-hero"}/>
                        <div className="content-workout">
                            {getContentWorkout()}
                        </div>
                    </>
                }
            </div>
        </div>
    )
}

export default WorkoutMultiPage;

