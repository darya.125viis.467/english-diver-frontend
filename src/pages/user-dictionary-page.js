import {useState} from "react";

import ModalMenu from "../components/modal-menu/modal-menu";
import UserDictionaryWordsList from "../components/user-dictionary/user-dictionary-words-list/user-dictionary-words-list";
import NavigationButtons from "../components/navigation-buttons/navigation-buttons";
import Footer from "../components/footers/footer/footer";
import InitialHeader from "../components/initial-header/initial-header";
import WordSearch from "../components/word-search/word-search";
import ButtonBar from "../components/user-dictionary/button-bar/button-bar";

const UserDictionaryPage = () => {

    const [modalActive, setModalActive] = useState(false);

    const [term, setTerm] = useState('');

    const [totalCountWords, setTotalCountWords] = useState(0);
    const [listWords, setListWords] = useState([]);

    const [showButtonBar, setShowButtonBar] = useState(false);
    const [isCheckAll, setIsCheckAll] = useState(false);
    const [isCheck, setIsCheck] = useState([]);

    const toggleModalActive = () => {
        setModalActive(modalActive => !modalActive)
    }

    const toggleShowButtonBar= () => {
        setShowButtonBar(showButtonBar => !showButtonBar);
    }

    const handleClick = e => {
        const {name, checked} = e.target;
        setIsCheck([...isCheck, name]);
        if (!checked) {
            setIsCheck(isCheck.filter(item => item !== name));
        }
    };

    const handleSelectAll = () => {
        setIsCheckAll(!isCheckAll);
        setIsCheck(listWords.map(item => item.word.id));
        if (isCheckAll) {
            setIsCheck([]);
        }
    };

    const countWordsHeading = "Всего слов на изучении: " + totalCountWords;

    return (
        <>
            <ModalMenu active={modalActive}
                       setActive={toggleModalActive}/>
            <div className="container">
                <InitialHeader firstHeading="Ваши слова для обучения"
                               secondHeading={countWordsHeading}/>
                <div className="row" style={{"margin-bottom": "25px"}}>
                    <WordSearch term={term}
                                setTerm={setTerm}/>
                    <ButtonBar handleSelectAll={handleSelectAll}
                               isCheck={isCheck}
                               isCheckAll={isCheckAll}
                               showButtonBar={showButtonBar}
                               toggleShowButtonBar={toggleShowButtonBar}/>
                </div>
                <UserDictionaryWordsList term={term}
                                         listWords={listWords}
                                         setListWords={setListWords}
                                         handleClick={handleClick}
                                         isCheck={isCheck}
                                         setIsCheck={setIsCheck}
                                         totalCountWords={totalCountWords}
                                         setTotalCountWords={setTotalCountWords}
                                         showButtonBar={showButtonBar}/>
                <NavigationButtons firstButtonName="Наборы слов"
                                   firstLink="/word-sets"
                                   secondButtonName="Тренировки"
                                   secondLink="/"/>
                <Footer/>
            </div>
        </>
    )
}

export default UserDictionaryPage;