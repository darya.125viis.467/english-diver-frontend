import LoginHeader from "../components/authorization/login-header/login-header";
import RegisterForm from "../components/authorization/register-form/register-form";
import AuthFooter from "../components/footers/auth-footer/auth-footer";

const RegistrationPage = () => {
    return (
        <div className="backg">
            <LoginHeader/>
            <RegisterForm/>
            <AuthFooter/>
        </div>
    )
}

export default RegistrationPage;