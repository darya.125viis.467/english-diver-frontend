import {useEffect, useState} from "react";
import {Link} from "react-router-dom";

import ContentWorkout from "../../components/workouts/workout-quick-translation/content-workout/content-workout";
import Timer from "../../components/workouts/workout-quick-translation/timer/timer";
import WorkoutHeader from "../../components/workouts/workout-header/workout-header";
import WorkoutResult from "../../components/workouts/workout-result/workout-result";

import useWorkout from "../../hooks/useWorkout";
import useHttpWorkout from "../../hooks/useHttpWorkout";

import "./workout-quick-translation-page.css"


const WorkoutQuickTranslation = () => {

    const [correctAnswerBool, setCorrectAnswerBool] = useState(null);

    const [responseMessage, setResponseMessage] = useState("");
    const [disabledButtons, setDisabledButtons] = useState(false);

    const [totalWorkoutPage, setTotalWorkoutPage] = useState(15);

    const {
        dataForWorkout,
        changeAnswerList,
        sendResultsToServer,
        loading,
        error
    } = useHttpWorkout("fast-translate");

    const {
        word,
        translation,
        similarWords,
        currentWorkoutPage,
        numCorrectAnswers,
        changeCurrentWorkoutPage,
        changeNumCorrectAnswers,
        getDataForWorkoutPage
    } = useWorkout(dataForWorkout);

    useEffect(() => {
        if(dataForWorkout){
            setTotalWorkoutPage(dataForWorkout.length);
        }
    }, [dataForWorkout])

    useEffect(() => {
        if (dataForWorkout && currentWorkoutPage <= totalWorkoutPage) {
            getDataForWorkoutPage();
        } else if (currentWorkoutPage > totalWorkoutPage){
            sendResultsToServer();
        }
    }, [currentWorkoutPage, dataForWorkout])


    return (
        <div className="preloading-workout-quick">
            <Link to="/"><div className="right-close">Выйти</div></Link>
            <div className="container">
                { currentWorkoutPage > totalWorkoutPage ?
                    <WorkoutResult totalWorkoutPage={totalWorkoutPage}
                                   numCorrectAnswers={numCorrectAnswers}
                                   styleContent={"content-workout-q"}
                                   styleButton={"btn-modal-q"}/> :
                    <>
                        <Timer correctAnswerBool={correctAnswerBool}
                               setCorrectAnswerBool={setCorrectAnswerBool}
                               currentWorkoutPage={currentWorkoutPage}
                               setResponseMessage={setResponseMessage}/>
                        <WorkoutHeader loading={loading}
                                       header={"Перевод на скорость"}
                                       totalNumber={totalWorkoutPage}
                                       number={currentWorkoutPage}
                                       styleHeader={"works-hero-workout-quick"}/>
                        <ContentWorkout error={error}
                                        loading={loading}
                                        word={word}
                                        changeAnswerList={changeAnswerList}
                                        translation={translation}
                                        similarWords={similarWords}
                                        responseMessage={responseMessage}
                                        setResponseMessage={setResponseMessage}
                                        disabledButtons={disabledButtons}
                                        setDisabledButtons={setDisabledButtons}
                                        setCorrectAnswerBool={setCorrectAnswerBool}
                                        changeCurrentWorkoutPage={changeCurrentWorkoutPage}
                                        changeNumCorrectAnswers={changeNumCorrectAnswers}/>
                    </>
                }
            </div>
        </div>
    )
}

export default WorkoutQuickTranslation;

