import {Redirect, Route} from "react-router-dom";

import AuthService from '../services/auth-service';

const PrivateRoute = ({ children, path }) => {
    const user = AuthService.getCurrentUserToken();

    let elem;

    if(user){
        elem = children;
    } else {
        elem = <Redirect to='/authorization' />;
    }

    return <Route path={path}>{elem}</Route>
}

export default PrivateRoute;